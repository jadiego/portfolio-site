# portfolio-site
Repo for portfolio site, using [Gatsby](https://www.gatsbyjs.org/).

## Running in development
`gatsby develop`
or 
`npm run develop`
