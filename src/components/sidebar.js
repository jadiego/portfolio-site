import React, { Component } from "react";

import Container from "./container";
import resume from "../assets/resume.pdf";

export default class Sidebar extends Component {
  render() {
    return (
      <Container>
        <div className="short-description">
          <div>
            Software Developer based in Seattle, WA, who loves to travel, game
            occasionally, and follow basketball ardently.
          </div>
          <br />
          <div>⛰ 🎮 🏀</div>
          <br />
        </div>

        <div>
          <ul className="links">
            <li>
              <a href="https://github.com/jadiego">github →</a>
            </li>
            <li>
              <a href="https://www.linkedin.com/in/johndiego/">linkedin →</a>
            </li>
            <li>
              <a href="mailto:johnadiego@gmail.com">email →</a>
            </li>
            <li>
              <a href={resume} target="blank">resume →</a>
            </li>
          </ul>
        </div>
        <p>Copyright © 2018 John Diego —</p>
      </Container>
    );
  }
}
