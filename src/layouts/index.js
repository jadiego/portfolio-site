import React from "react";
import Sidebar from "../components/sidebar";

import "../styles/main.scss";

export default ({ children }) => (
  <div className="app-container">
    <div className="content-region">{children()}</div>
    <div className="sidebar-region">
      <Sidebar />
    </div>
  </div>
);
