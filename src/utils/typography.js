import Typography from "typography";
import irvingTheme from "typography-theme-irving";

irvingTheme.baseFontSize = "20px";
irvingTheme.scaleRatio = 1.7;
irvingTheme.baseLineHeight = 1.42;
irvingTheme.bodyColor = "hsla(0,0%,0%,0.73)"

irvingTheme.overrideThemeStyles = ({ rhythm }, options) => ({
  ".subtle-title": {
    fontSize: rhythm(0.9/2),
    letterSpacing: ".15em",
    color: "hsla(0,0%,0%,0.3)",
    textTransform: "uppercase",
  },
  "a": {
    textDecoration: "none",
    borderBottomWidth: "1px",
    borderBottomStyle: "dashed",
    color: "#738BD7"
  },
  "a:hover": {
    borderBottomStyle: "solid",
    color: "#ABC0FF"
  },
})

const typography = new Typography(irvingTheme);

export default typography;
