import React from "react";
import Container from "../components/container";

export default ({ data }) => (
  <Container>
    <div className="main-content-container">
      <h1>
        👋 Hello world, I'm <span>John</span>
      </h1>

      <section className="aboutme">
        <h6 className="subtle-title">about me</h6>
        <p>
          I'm a recent graduate from the University of Washington with a B.S. in
          Informatics, focusing in Software Development. I mostly focus on
          full-stack development, and have made some{" "}
          <a
            target="_blank"
            href="https://ischool.uw.edu/news/2017/06/capstone-team-works-restore-trauma-survivors-voices"
          >
            pretty cool things
          </a>, but I'm ready to take things to the next level.
        </p>
        <div className="alert">
          <span>
            Currently, I am{" "}
            <strong>interested in internships or full time positions</strong> in
            full stack development for <strong>fall 2018</strong>!
          </span>
        </div>
      </section>

      <section className="projects">
        <h6 className="subtle-title">projects</h6>
        {data.site.siteMetadata.projects.map((project, index) => (
          <div className="card" key={index}>
            <h3>{project.title}</h3>
            <p>{project.description}</p>
            {project.link.length > 0 && <a href={project.link}>learn more</a>}
          </div>
        ))}
      </section>
    </div>
  </Container>
);

export const query = graphql`
  query ProjectsQuery {
    site {
      siteMetadata {
        projects {
          title
          description
          link
        }
      }
    }
  }
`;
