module.exports = {
  siteMetadata: {
    projects: [
      {
        title: "react-slack-clone",
        description: `An instant messaging web application, similar to Slack, 
        that features a microservice architecture and a React-Typescript web client.`,
        link: "https://github.com/jadiego/react-slack-clone"
      },
      {
        title: "bloom",
        description: `Bloom is a web-based storytelling platform made in collaboration
        with API Chaya that allows users to create and share personal
        stories.`,
        link: "https://github.com/jadiego/bloom"
      },
      {
        title: "uwrouting",
        description: `A Chrome/Firefox extension I made as a student email analyst at 
        the UW-IT Service Center. It enables extra keyboard shortcuts on the internal 
        ticketing system web interface.`,
        link: ""
      },
      {
        title: "imgrab",
        description: `A Chrome extension that allows the user to grab a screenshot 
        of the current tab or selected area and upload it to imgur anonymously.`,
        link: "https://github.com/jacobdevera/imgrab"
      }
    ]
  },
  plugins: [
    {
      resolve: "gatsby-plugin-typography",
      options: {
        pathToConfigModule: "src/utils/typography.js"
      }
    },
    "gatsby-plugin-sass"
  ]
};
